/*
 * @Author: 鸿羽飞舟 2653235481@qq.com
 * @Date: 2022-08-19 09:14:53
 * @LastEditors: 鸿羽飞舟 2653235481@qq.com
 * @LastEditTime: 2022-08-19 09:25:46
 * @FilePath: \学习笔记-1c:\Users\zhongyuhong\Documents\Tencent Files\2653235481\FileRecv\实训三\js刷题\09.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
// 给定一个只包括 '('，')'，'{'，'}'，'['，']' 的字符串 s ，判断字符串是否有效。       有效字符串需满足：左括号必须用相同类型的右括号闭合。左括号必须以正确的顺序闭合。
var isValid = function(s) {
    var reg1=/\(\)/g;
    var reg2=/\[\]/g;
    var reg3=/\{\}/g;
    var len=s.length;
    for(var i=0;i<len;i++){
        if(reg1.test(s)==true){
            s=s.split('()').join('');
        }else if(reg2.test(s)==true){
            s=s.split('[]').join('');
        }else if(reg3.test(s)==true){
            s=s.split('{}').join('');
        }
    }
    if(s.length==0){
        return true;
    }else{
        return false;
    }
};
var s = "()[]{}"
console.log(isValid(s));
console.log(isValid('{([({)}])}[()]'));