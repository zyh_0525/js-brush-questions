/*
 * @Author: 鸿羽飞舟 2653235481@qq.com
 * @Date: 2022-08-19 09:14:45
 * @LastEditors: 鸿羽飞舟 2653235481@qq.com
 * @LastEditTime: 2022-08-19 09:18:56
 * @FilePath: \学习笔记-1c:\Users\zhongyuhong\Documents\Tencent Files\2653235481\FileRecv\实训三\js刷题\07.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
//*罗马数字转整数
function romanToInt(s) {
    var n=s.split('');
    var sum=[];
    var x=0;
    for(var i=0;i<n.length;i++){
        switch(n[i]){
            case 'I':
                sum[i]=1;
                break;
            case 'V':
                sum[i]=5;
                break;
            case 'X':
                sum[i]=10;
                break;
            case 'L':
                sum[i]=50;
                break;
            case 'C':
                sum[i]=100;
                break;
            case 'D':
                sum[i]=500;
                break;
            case 'M':
                sum[i]=1000;
                break;
        }
    }
    for(var j=0;j<sum.length;j++){
        if(sum[j]<sum[j+1]){
            x+=sum[j+1]-sum[j];
            j=j+1;
        }else{
            x+=sum[j];
        }
    }
    return x;
};
var  s = "MCMXCIV"
console.log(romanToInt(s));
console.log(romanToInt('MMXXII'));