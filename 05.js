/*
 * @Author: 鸿羽飞舟 2653235481@qq.com
 * @Date: 2022-08-19 09:03:41
 * @LastEditors: 鸿羽飞舟 2653235481@qq.com
 * @LastEditTime: 2022-08-19 09:11:59
 * @FilePath: \学习笔记-1c:\Users\zhongyuhong\Documents\Tencent Files\2653235481\FileRecv\实训三\js刷题\05.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
// *给你一个整数 x ，如果 x 是一个回文整数，返回 true ；否则，返回 false 。      回文数是指正序（从左向右）和倒序（从右向左）读都是一样的整数。
function isPalindrome (x) {
    var x1=x.toString().split('');
    var sum=x1[x1.length-1];
    // console.log(sum);
    for(var i=x1.length-2;i>=0;i--){
        sum +=x1[i];
    }
    if(x.toString()==sum){
        return true;
    }else{
        return false;
    }
};
var x=122;
console.log(isPalindrome (x));
console.log(isPalindrome (121));