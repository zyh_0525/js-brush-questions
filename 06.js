/*
 * @Author: 鸿羽飞舟 2653235481@qq.com
 * @Date: 2022-08-19 09:03:45
 * @LastEditors: 鸿羽飞舟 2653235481@qq.com
 * @LastEditTime: 2022-08-19 09:14:07
 * @FilePath: \学习笔记-1c:\Users\zhongyuhong\Documents\Tencent Files\2653235481\FileRecv\实训三\js刷题\06.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
// *给定一个由整数组成的非空数组所表示的非负整数，在该数的基础上加一。
// digits = [1,2,3]
// 输出：[1,2,4]
function plusOne(digits) {
    var n=digits.reverse();
    n[0]=Number(n[0]+1);
    for(var i=0;i<=n.length-1;i++){
        if(n[i]==10){
            n[i]=0;
            if(n[i+1]==undefined){
                n[i+1]=1;
            }else{
                n[i+1]=n[i+1]+1;
            }
        }
    }
    return n.reverse();
};
digits = [9]
console.log(plusOne(digits));
console.log(plusOne([1,2,3]));
