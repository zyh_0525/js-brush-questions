/*
 * @Author: 鸿羽飞舟 2653235481@qq.com
 * @Date: 2022-08-19 09:44:50
 * @LastEditors: 鸿羽飞舟 2653235481@qq.com
 * @LastEditTime: 2022-08-19 09:48:09
 * @FilePath: \学习笔记-1c:\Users\zhongyuhong\Documents\Tencent Files\2653235481\FileRecv\实训三\js刷题\17.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
// *零矩阵     编写一种算法，若M × N矩阵中某个元素为0，则将其所在的行与列清零。（简单粗暴版）
var setZeroes = function(matrix) {
    var a=[],b=[];
    for(var n=0;n<matrix.length;n++){
        for(var m=0;m<matrix[0].length;m++){
            if(matrix[n][m]==0){
                a.push(n);
                b.push(m)
            }
        }
    }
    for(var i in matrix[0]){
        for(var j in a){
            matrix[a[j]][i]=0;
        }
    }
    for(var j in matrix){
        for(var k in b){
            matrix[j][b[k]]=0;
        }
    }
    return matrix
};
var matrix=[[1,1,1],[1,0,1],[1,1,1],[1,0,0]];
console.log(setZeroes(matrix));