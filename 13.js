/*
 * @Author: 鸿羽飞舟 2653235481@qq.com
 * @Date: 2022-08-19 09:39:43
 * @LastEditors: 鸿羽飞舟 2653235481@qq.com
 * @LastEditTime: 2022-08-19 09:42:41
 * @FilePath: \学习笔记-1c:\Users\zhongyuhong\Documents\Tencent Files\2653235481\FileRecv\实训三\js刷题\13.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
// *寻找数组的中心索引.                注：给你一个整数数组 nums ，请计算数组的 中心下标 。
var pivotIndex = function(nums) {
    var sum1=0,sum2=0;
    nums[-1]=0;
    nums[nums.length]=0;
    for(var i=0;i<nums.length-1;i++){
        sum1=0,sum2=0;
        for(var j=0;j<nums.length-1;j++){
            if(j<i){
                sum1+=nums[j];
            }else if(j>i){
                sum2+=nums[j];
            }
        }
        if(sum1==sum2){
            return i
        }
    }
    return -1;
};
var nums=[1,7,3,6,5,6];
console.log(pivotIndex(nums));              //3
console.log(pivotIndex([2,1,-1]));          //0
console.log(pivotIndex([-10,4,5,1]));       //-1