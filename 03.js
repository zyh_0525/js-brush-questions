/*
 * @Author: 鸿羽飞舟 2653235481@qq.com
 * @Date: 2022-08-19 09:03:33
 * @LastEditors: 鸿羽飞舟 2653235481@qq.com
 * @LastEditTime: 2022-08-19 09:09:29
 * @FilePath: \学习笔记-1c:\Users\zhongyuhong\Documents\Tencent Files\2653235481\FileRecv\实训三\js刷题\03.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
// *给定两个大小分别为 m 和 n 的正序（从小到大）数组 nums1 和 nums2。请你找出并返回这两个正序数组的 中位数 。
// 输入：nums1 = [1,3], nums2 = [2]
// 输出：2.00000
// 解释：合并数组 = [1,2,3] ，中位数 2
function findMedianSortedArrays(nums1, nums2){
    var nums3=(nums1.toString()+','+nums2.toString()).split(',');
    for(var i=1;i<nums3.length;i++){
        for(var j=0;j<i;j++){
            if(nums3[i]<nums3[j]){
                var tmp =nums3[i];
                nums3[i]=nums3[j];
                nums3[j]=tmp;
            }
        }
    }
    var res;
    if(nums3.length%2!=0){
        res=Number(nums3[(nums3.length-1)/2]);
    }else{
        res=(Number(nums3[nums3.length/2-1])+Number(nums3[nums3.length/2]))/2;
    }
    res=res.toFixed(5);
    return res;
};
nums1 = [1,3], nums2 = [2];
console.log(findMedianSortedArrays(nums1, nums2));