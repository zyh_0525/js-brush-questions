/*
 * @Author: 鸿羽飞舟 2653235481@qq.com
 * @Date: 2022-08-19 09:27:52
 * @LastEditors: 鸿羽飞舟 2653235481@qq.com
 * @LastEditTime: 2022-08-19 09:38:05
 * @FilePath: \学习笔记-1c:\Users\zhongyuhong\Documents\Tencent Files\2653235481\FileRecv\实训三\js刷题\11.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
// 查找haystack字符串中是否有满足needle的字符串，有返回第一次出现下标，没有返回-1
var strStr = function(haystack, needle) {
    var reg=new RegExp(needle,"g");
    var a=reg.exec(haystack);
    if(a!=null){
        return a["index"];
    }else{
        return -1;
    }
};
var haystack = "aaaaa", needle = "bba";
console.log(strStr(haystack, needle));
console.log(strStr('abcdeab', 'cd'));