/*
 * @Author: 鸿羽飞舟 2653235481@qq.com
 * @Date: 2022-08-19 09:44:44
 * @LastEditors: 鸿羽飞舟 2653235481@qq.com
 * @LastEditTime: 2022-08-19 09:46:14
 * @FilePath: \学习笔记-1c:\Users\zhongyuhong\Documents\Tencent Files\2653235481\FileRecv\实训三\js刷题\16.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
// *旋转矩阵       给你一幅由 N × N 矩阵表示的图像，其中每个像素的大小为 4 字节。请你设计一种算法，将图像旋转 90 度。
var rotate = function(matrix) {
    for (let i = 0; i < matrix.length / 2; i++) {
        let temp = matrix[i];
        matrix[i] = matrix[matrix.length - i - 1];
        matrix[matrix.length - i - 1] = temp;
    }
    for (let i = 0; i < matrix.length; ++i) {
        for (let j = i + 1; j < matrix.length; ++j) {
            let temp = matrix[i][j];
            matrix[i][j] = matrix[j][i];
            matrix[j][i] = temp;
        }
    }
    return matrix;
};
var matrix=[[1,2,3],[4,5,6],[7,8,9]];
console.log(rotate(matrix));

// 法二：
var rotate = function(matrix) {
var n=[]
    for(var i in matrix){
        var a=[]
        n.push(a);
        for(var j in matrix){
            a.push(matrix[matrix.length-1-j][i])
        }
    }
    return n
};
var matrix=[[1,2,6],[9,5,7],[17,8,69]];
console.log(rotate(matrix));