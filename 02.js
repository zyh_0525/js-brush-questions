/*
 * @Author: 鸿羽飞舟 2653235481@qq.com
 * @Date: 2022-08-19 09:03:28
 * @LastEditors: 鸿羽飞舟 2653235481@qq.com
 * @LastEditTime: 2022-08-19 09:11:01
 * @FilePath: \学习笔记-1c:\Users\zhongyuhong\Documents\Tencent Files\2653235481\FileRecv\实训三\js刷题\02.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
// *两数相加  输入：l1 = [2,4,3], l2 = [5,6,4]
// 输出：[7,0,8]
// 解释：342 + 465 = 807.
function addTwoNumbers(l1, l2) {
    var sum1=l1[l1.length-1],sum2=l2[l2.length-1];
    var sum;
    for(var i=l1.length-2;i>=0;i--){
        sum1+=l1[i]+'';
    }
    for(var j=l2.length-2;j>=0;j--){
        sum2+=l2[j]+'';
    }
    sum=(Number(sum1)+Number(sum2)).toString().split('');
    // console.log(sum);
    /*for(var k=0;k<sum.length/2;k++){
        var tmp=sum[k];
        sum[k]=sum[sum.length-1-k]
        sum[sum.length-1-k]=tmp;
    }*/
    sum=(Number(sum1)+Number(sum2)).toString().split('').reverse();
    return sum;
};
var l1 = [2,4,3], l2 = [5,6,4];
console.log(addTwoNumbers(l1,l2));