/*
 * @Author: 鸿羽飞舟 2653235481@qq.com
 * @Date: 2022-08-19 09:27:56
 * @LastEditors: 鸿羽飞舟 2653235481@qq.com
 * @LastEditTime: 2022-08-19 09:38:28
 * @FilePath: \学习笔记-1c:\Users\zhongyuhong\Documents\Tencent Files\2653235481\FileRecv\实训三\js刷题\12.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
// 已有顺序排序的数组，请数组去重
var removeDuplicates = function(nums) {
    var b=0;
    for(var i=0;i<nums.length;i++){
        var a=nums.indexOf(nums[i]);
        // console.log(i,a);
        if(a==i){
            nums[b]=nums[a];
            b++;
        }
    }
    nums.length=b;
    return nums
};
var nums=[0,0,1,1,1,2,2,3,3,4]
console.log(removeDuplicates(nums));