/*
 * @Author: 鸿羽飞舟 2653235481@qq.com
 * @Date: 2022-08-19 09:39:53
 * @LastEditors: 鸿羽飞舟 2653235481@qq.com
 * @LastEditTime: 2022-08-19 09:44:04
 * @FilePath: \学习笔记-1c:\Users\zhongyuhong\Documents\Tencent Files\2653235481\FileRecv\实训三\js刷题\15.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
// *合并区间           以数组 intervals 表示若干个区间的集合，其中单个区间为 intervals[i] = [starti, endi] 。请你合并所有重叠的区间，并返回 一个不重叠的区间数组，该数组需恰好覆盖输入中的所有区间 。
var merge = function (intervals) {
    for (var j = 0; j < intervals.length; j++) {
        for (var i = 1; i < intervals.length; i++) {
            var a1 = intervals[i - 1][0], a2 = intervals[i][0], b1 = intervals[i - 1][1], b2 = intervals[i][1];
            if (a1 < a2) {
                if (b1 >= a2) {
                    // console.log('1');
                    intervals[i] = b1 >= b2 ? [a1, b1] : [a1, b2];
                    intervals.splice(i - 1, 1);
                    i--;
                }
            } else if (a2 < a1) {
                if (b2 >= a1) {
                    // console.log('2');
                    intervals[i] = b1 >= b2 ? [a2, b1] : [a2, b2];
                    intervals.splice(i - 1, 1);
                    i--;
                    j--;
                } else {
                    // console.log('3');
                    var tmp = intervals[i - 1];
                    intervals[i - 1] = intervals[i];
                    intervals[i] = tmp;
                }
            } else {
                // console.log('4');
                if (b1 == b2) {
                    intervals.splice(i - 1, 1);
                    i--;
                } else {
                    intervals[i] = b1 > b2 ? [a1, b1] : [a1, b2];
                    intervals.splice(i - 1, 1);
                    i--;
                }
            }
            // console.log(intervals,i);
        }
    }
    return intervals
};
var intervals = [[2, 3], [4, 5], [6, 7], [8, 9], [1, 10]];
console.log(merge(intervals));