/*
 * @Author: 鸿羽飞舟 2653235481@qq.com
 * @Date: 2022-08-19 09:39:48
 * @LastEditors: 鸿羽飞舟 2653235481@qq.com
 * @LastEditTime: 2022-08-19 09:42:48
 * @FilePath: \学习笔记-1c:\Users\zhongyuhong\Documents\Tencent Files\2653235481\FileRecv\实训三\js刷题\14.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
// *搜索插入位置           给定一个排序数组和一个目标值，在数组中找到目标值，并返回其索引。如果目标值不存在于数组中，返回它将会被按顺序插入的位置。
var searchInsert = function(nums, target) {
    for(var i=0;i<nums.length;i++){
        if(nums[i]>=target){
            return i;
        }
    }
    return nums.length;
};
var  nums = [1,3,5,6], target = 7;
console.log(searchInsert(nums, target));        //4
console.log(searchInsert(nums, 5));             //2
