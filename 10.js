/*
 * @Author: 鸿羽飞舟 2653235481@qq.com
 * @Date: 2022-08-19 09:27:45
 * @LastEditors: 鸿羽飞舟 2653235481@qq.com
 * @LastEditTime: 2022-08-19 09:32:32
 * @FilePath: \学习笔记-1c:\Users\zhongyuhong\Documents\Tencent Files\2653235481\FileRecv\实训三\js刷题\10.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
// 判断是否为单调函数（单增/单减/始终不变）
var isMonotonic = function(nums) {
    var a=nums.length-1;
    for(var i=0;i<a;i++){
        if(nums[0]-nums[a]>0){
            if(nums[i]<nums[i+1]){
                return false;
            }
        }else if(nums[0]-nums[a]<0){
            if(nums[i]>nums[i+1]){
                return false;
            }
        }else{
            if(nums[i]!=nums[i+1]){
                return false;
            }
        }
    }
    return true;
};
var nums = [3,3,3,3];
console.log(isMonotonic(nums));             //true
console.log(isMonotonic([1,2,3,4]));        //true
console.log(isMonotonic([9,3,6,1]));        //false
console.log(isMonotonic([9,6,1]));          //true
console.log(isMonotonic([3,3,3,5,8,3]));    //false